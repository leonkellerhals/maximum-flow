package maximumflow;

public class Edge {
	// Kapazitaet
	private String src;
	private String dest;
	private int maxCapacity;
	private int actualCapacity;
	
	public Edge(String src, String dest, int maxCapacity) {
		this.src = src;
		this.dest = dest;
		this.maxCapacity = maxCapacity;
	}
	
	public Edge (Edge copy) {
		this.src = copy.src;
		this.dest = copy.dest;
		this.maxCapacity = copy.maxCapacity;
		this.actualCapacity = copy.actualCapacity;
	}
	
	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public int getMaxCapacity() {
		return maxCapacity;
	}

	public void setMaxCapacity(int maxCapacity) {
		this.maxCapacity = maxCapacity;
	}

	public int getActualCapacity() {
		return actualCapacity;
	}

	public void setActualCapacity(int actualCapacity) {
		this.actualCapacity = actualCapacity;
	}

	@Override
	public String toString() {
		return "<" + this.src + ", " + this.dest + ", " + 
		this.maxCapacity + ", " + this.actualCapacity + ">";
	}
		
	public String toStringWithoutCapacity() {
		return "<" + this.src + ", " + this.dest + ", " + 
				this.maxCapacity + ">";
	}
	
}
