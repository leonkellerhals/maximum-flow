package maximumflow;
import java.util.ArrayList;


public class Graph {
	
	// Alle Kanten des Graphen
	private ArrayList<Edge> edges = new ArrayList<Edge>();
	
	public Graph() { }
	
	public Graph(Graph copy) {
		for (Edge e : copy.edges) {
			this.edges.add(new Edge(e));
		}
	}
	
	public void addEdge(String src, String dest, int maxCapacity) throws EdgeAlreadyDefinedException {
		if (this.getEdge(src, dest) != null) {
			throw new EdgeAlreadyDefinedException();
		}
		getEdges().add(new Edge(src, dest, maxCapacity));
	}
	
	/**
	 * Gives you all Edges that have the given node as source. 
	 * @param node One of the nodes
	 * @return An ArrayList of Edges. Null, if the node was not found.
	 */
	public ArrayList<Edge> getAllEdgesWithNodeAsSrc(String node) {
		ArrayList<Edge> result = new ArrayList<Edge>();
		
		// Iteriere durch alle vorhandenen Kanten und suche nach Kanten, 
		// die den gegebenen Knoten als Quelle enthalten.
		for (Edge e : this.getEdges()) {
			if (e.getSrc().equals(node)) {
				result.add(e);
			}
		}
		if (result.isEmpty()) return null;
		return result;
	}
	/**
	 * Gives you all Edges that have the given node as destination. 
	 * @param node One of the nodes
	 * @return An ArrayList of Edges. Null, if the node was not found.
	 */
	public ArrayList<Edge> getAllEdgesWithNodeAsDest(String node) {
		ArrayList<Edge> result = new ArrayList<Edge>();
		
		// Iteriere durch alle vorhandenen Kanten und suche nach Kanten, 
		// die den gegebenen Knoten als Quelle enthalten.
		for (Edge e : this.getEdges()) {
			if (e.getDest().equals(node)) {
				result.add(e);
			}
		}
		if (result.isEmpty()) return null;
		return result;
	}
	
	/**
	 * Get the Edge with given source and destination
	 * @param src The source of the edge.
	 * @param dest The destination of the edge.
	 * @return The Object of Edge that is searched. Null if the Edge does not 
	 * exist.
	 */
	public Edge getEdge(String src, String dest) {
		
		//System.out.println("To be searched: " + src + ", " + dest);
		for (Edge e : this.getEdges()) {
			// Wenn Quelle und Ziel der Kante uebereinstimmen, dann handelt es 
			// sich um die gesuchte Kante.
			//System.out.println("Checked on: " + src + ", " + dest);
			if (e.getSrc().equals(src) && e.getDest().equals(dest)) {
				//System.out.println("true!");
				return e;
			}
		}
		// Wenn keine Kante gefunden worden ist, gebe null zurueck.
		return null;
	}
	
	/**
	 * Creates the residual graph on basis of the actualFlow values in the List
	 * of Edges
	 * @return the residual Graph of this object.
	 */
	public Graph createResidualGraph() {
		int newMaxCapacity;
		
		// Erstelle zunaechst eine Kopie des aktullen Graphen.  
		Graph residualGraph = new Graph(this);
		
		// Fuer jede Kante muessen zwei Operationen ausgefuehrt werden: 
		// Die Berechnung der neuen Maximalkapazitaet und das Erstellen der
		// Gegenkante.
		for (int i = 0; i < residualGraph.getEdges().size(); i++) {
			Edge e = residualGraph.getEdges().get(i);
			
			newMaxCapacity = e.getMaxCapacity() - e.getActualCapacity();
			e.setMaxCapacity(newMaxCapacity);
			
			// Pruefen, ob Gegenkante schon existiert:
			Edge ce = residualGraph.getEdge(e.getDest(), e.getSrc());
			if (ce != null) {
				// Gegenkante existiert bereits. Der Maximalfluss kann 
				// aufaddiert werden.
				newMaxCapacity = ce.getMaxCapacity() + e.getActualCapacity();
				ce.setMaxCapacity(newMaxCapacity);
			} else {
				// Gegenkante existiert noch nicht.
				newMaxCapacity = e.getActualCapacity();
				residualGraph.getEdges().add(new Edge(e.getDest(), e.getSrc(), newMaxCapacity));
			}
		}
		
		return residualGraph;
	}
	
	/**
	 * Returns this class in form of an adjacency list.
	 * @return the adjacency list of the graph.
	 */
	@Override
	public String toString() {
		String str= "";
		
		// Iteriere durch alle Kanten
		for (Edge e : this.getEdges()) {
			str += e.toString() + "\n";
		}
		
		return str;
	}
	
	/**
	 * Returns this class in form of an adjacency list.
	 * @return the adjacency list of the graph.
	 */
	public String toStringWithoutCapacity() {
		String str= "";
		
		// Iteriere durch alle Kanten
		for (Edge e : this.getEdges()) {
			str += e.toStringWithoutCapacity() + "\n";
		}
		
		return str;
	}

	public ArrayList<Edge> getEdges() {
		return edges;
	}

	public void setEdges(ArrayList<Edge> edges) {
		this.edges = edges;
	}
}

