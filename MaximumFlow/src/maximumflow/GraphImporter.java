package maximumflow;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


public class GraphImporter {
	
public Graph getGraph(File f) throws IOException {
		
		BufferedReader br = new BufferedReader(new FileReader(f));
		Graph g = new Graph();
		
		String line = null;
		
		String[] values = {"", "", "", ""};
		
		int valueCount;
		
		// Jede Zeile enth�lt ein Werttupel der Adjazenzliste
		while ((line = br.readLine()) != null) {
			
			for (int i = 0; i < values.length; i++) {
				values[i] = "";
			}
			valueCount = 0;
			
			for (int i = 0; i < line.length(); i++) {
				if (line.charAt(i) != (char) 9) {
					values[valueCount] += line.charAt(i);
				} else {
					valueCount++;
				}
			}
			
			try {
				g.addEdge(values[0], values[1], new Integer(values[3]));
			} catch (NumberFormatException e) {
				System.out.println("Fehler beim Knvertieren der " +
						"Maximalkapazitaet in einen Integerwert.");
			} catch (EdgeAlreadyDefinedException e) {
				System.out.println("Konfliktierende Kanten: ");
				System.out.println("Existierende Kante: " + 
						g.getEdge(values[0], values[1]).toString());
				System.out.println("Zu erstellende Kante: <" + values[0] + 
						", " + values[1] + ", " + values[3]);
			}
		}
		
		br.close();
		
		return g;
		
	}
	
}
