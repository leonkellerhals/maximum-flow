package maximumflow;
import java.util.ArrayList;
import java.util.LinkedList;


public class MaximumFlow {
	
	private Graph g;
	
	public MaximumFlow(Graph g) {
		this.setGraph(g);
	}
	
	public int calculateMaximumFlow(String source, String sink) {
		
		// Es wird in der folgenden Schleife immer ein Pfad und ein Graph 
		// benoetigt. Der erste "Inkrementnetzwerk" ist der eigentliche Graph. 
		LinkedList<String> path;
		Graph residualGraph = new Graph(this.g);
		
		// Iteriere, solange im Inkrementnetzwerk ein Pfad von der Quelle zur 
		// Senke existiert.  
		while ((path = this.BFS(residualGraph, source, sink)) != null) {
			// Ermittle den maximalen Fluss, der durch den Pfad moeglich ist.
			Integer maxFlow = null;
			LinkedList<Edge> edges = new LinkedList<Edge>();
			
			for (int i = 0; i < (path.size()-1); i++) {
				// Hole die Kante, die den Knoten mit dem darauffolgenden 
				// Knoten im Pfad miteinander verbindet. 
				Edge e = residualGraph.getEdge(path.get(i), path.get(i+1));
				edges.add(e);
				
				// Wenn der Maximalfluss groesser ist als die Kapazitaet des 
				// Knotens, so muss dieser auf die Kapazitaet verringert werden. 
				if (maxFlow == null || maxFlow >= e.getMaxCapacity()) {
					maxFlow = e.getMaxCapacity();
				}
			}
			
			// Trage nun den maximalen Fluss in die jeweiligen Kanten ein. 
			for (Edge e : edges) {
				residualGraph.getEdge(e.getSrc(), e.getDest())
						.setActualCapacity(maxFlow);
				
				int addedFlow = this.g.getEdge(e.getSrc(), e.getDest())
						.getActualCapacity() + maxFlow;
				this.g.getEdge(e.getSrc(), e.getDest())
						.setActualCapacity(addedFlow);
			}
			
			// Erstelle nun das Inkrementnetzwerk. 
			residualGraph = new Graph(residualGraph.createResidualGraph());
			
			// R�cksetzen der "actualCapacity", um ein doppeltes Erstellen des 
			// Inkrementnetzwerkes zu vermeiden
			for (Edge e : edges) {
				residualGraph.getEdge(e.getSrc(), e.getDest()).setActualCapacity(0);
			}
		}
	
		// Berechne den an der Senke ankommenden Fluss (und damit den maximalen
		// Fluss
		int maximumFlow = 0;
		
		for (Edge e : this.g.getAllEdgesWithNodeAsDest(sink)) {
			maximumFlow += e.getActualCapacity();
		}
		
		return maximumFlow;
	}
	
	/**
	 * Returns the shortest path from startNode to endNode in the given Graph.
	 * @param g the graph to be searched in
	 * @param startNode the start node (must be in graph)
	 * @param endNode the end node (must be in graph)
	 * @return A Linked List of Strings that contain the Nodes of the path.
	 */
	private LinkedList<String> BFS(Graph g, String startNode, String endNode) {
		// Abzuarbeitende Warteschlange. Hierbei werden Pfade gespeichert, 
		// um zum Schluss einen Pfad zurueckgeben zu koennen. 
		LinkedList<LinkedList<String>> pathQueue = new LinkedList<LinkedList<String>>();
		
		ArrayList<String> searchedNodes = new ArrayList<String>();
		
		// Der erste Pfad enthaelt nur den Quellknoten. 
		LinkedList<String> startPath = new LinkedList<String>();
		startPath.add(startNode);
		pathQueue.add(startPath);
		
		while (!pathQueue.isEmpty()) {
			// Entnehme den naechsten Pfad aus der Warteschlange, 
			// nehme davon den letzten Knoten ...
			LinkedList<String> path = pathQueue.pop();
			String node = path.peekLast();
			
			
			// ... und ueberpruefe die von ihm wegfuehrenden Kanten:
			ArrayList<Edge> allEdgesOfNode = g.getAllEdgesWithNodeAsSrc(node);
			if (allEdgesOfNode == null) continue;
			
			for (Edge e : allEdgesOfNode) {
				
				// Pruefe, ob der Knoten bereits ueberprueft worden ist.
				if (searchedNodes.contains(e.getDest())) {
					continue;
				}
				
				// Pruefe, ob die Kapazitaet der Kante auch groesser als 0 ist.
				if (e.getMaxCapacity() <= 0) {
					// Wenn nicht, dann ist diese Kante nicht als solche zu 
					// betrachten.
					continue;
				}
				searchedNodes.add(e.getDest());
				
				// Erstelle neuen Pfad und den haenge den gefundenen Knoten an.
				LinkedList<String> newPath = (LinkedList<String>) path.clone();
				newPath.add(e.getDest());
				
				// Pruefe, ob der neue Knoten der Zielknoten ist: 
				if (e.getDest().equals(endNode)) {
					// Und gib den neuen Pfad in diesem Fall als Resultat zurueck. 
					return newPath;
				}
				
				// Ansonsten fuege den neuen Pfad an die Warteschlange. 
				pathQueue.add(newPath);
			}
		}
		
		// Wenn die Warteschlange abgearbeitet worden ist, 
		// gibt es keinen Pfad vom Startknoten zum Zielknoten. 
		return null;
	}
	
	/**
	 * Returns the shortest path from startNode to endNode in the given Graph.
	 * @param startNode the start node (must be in graph)
	 * @param endNode the end node (must be in graph)
	 * @return A Linked List of Strings that contain the Nodes of the path.
	 */
	public LinkedList<String> BFS(String startNode, String endNode) {
		// Nur fuer das Testen und fuer Uebersichtlichkeit hinzugefuegt.
		return this.BFS(this.g, startNode, endNode);
	}

	public Graph getGraph() {
		return g;
	}

	public void setGraph(Graph g) {
		this.g = g;
	}
}
