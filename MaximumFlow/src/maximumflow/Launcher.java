package maximumflow;
import java.io.File;
import java.io.IOException;

public class Launcher {
	
	public static void main(String args[]) {
		
		try {
			Graph g = new GraphImporter().getGraph(new File("Daten11A.txt"));
			System.out.println("Importierter Graph: \n" + g.toStringWithoutCapacity());
			
			MaximumFlow mf = new MaximumFlow(g);
			
			int maxFlow = mf.calculateMaximumFlow("6", "17");
			
			System.out.println("Maximaler Fluss: " + maxFlow);
			
			System.out.println("Benutzte Kapazitaeten: ");
			System.out.println(mf.getGraph());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
