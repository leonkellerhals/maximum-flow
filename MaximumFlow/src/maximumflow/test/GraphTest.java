package maximumflow.test;

import static org.junit.Assert.*;

import maximumflow.EdgeAlreadyDefinedException;
import maximumflow.Graph;

import org.junit.Test;

public class GraphTest {

	@Test
	public void testCreateResidualGraph() {
		
		Graph g = new Graph();
		
		try {
			g.addEdge("1", "2", 5);
			g.addEdge("1", "3", 3);
			g.addEdge("2", "3", 4);
			g.addEdge("3", "2", 2);
			g.addEdge("3", "5", 6);
			g.addEdge("4", "6", 1);
			g.addEdge("5", "4", 3);
			g.addEdge("5", "6", 2);
			
			g.getEdge("1", "2").setActualCapacity(2);
			g.getEdge("2", "3").setActualCapacity(2);
			g.getEdge("3", "5").setActualCapacity(2);
			g.getEdge("5", "6").setActualCapacity(2);
			
			Graph newGraph = g.createResidualGraph();
			
			//System.out.println(newGraph.toString());
			
			assertEquals(3, newGraph.getEdge("1", "2").getMaxCapacity());
			assertEquals(2, newGraph.getEdge("2", "3").getMaxCapacity());
			assertEquals(4, newGraph.getEdge("3", "5").getMaxCapacity());
			assertEquals(0, newGraph.getEdge("5", "6").getMaxCapacity());
			assertEquals(2, newGraph.getEdge("2", "1").getMaxCapacity());
			assertEquals(4, newGraph.getEdge("3", "2").getMaxCapacity());
			assertEquals(2, newGraph.getEdge("5", "3").getMaxCapacity());
			assertEquals(2, newGraph.getEdge("6", "5").getMaxCapacity());
			
			
		} catch (EdgeAlreadyDefinedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetEdgeWithNullResult() {
		Graph g = new Graph();
		
		try {
			g.addEdge("1", "2", 5);
			g.addEdge("1", "3", 3);
			g.addEdge("2", "3", 4);
			g.addEdge("3", "2", 2);
			g.addEdge("3", "5", 6);
			g.addEdge("4", "6", 1);
			g.addEdge("5", "4", 3);
			g.addEdge("5", "6", 2);
			
			assertNull(g.getEdge("1", "6"));
		} catch (EdgeAlreadyDefinedException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetEdgeWithRealResult() {
		Graph g = new Graph();
		
		try {
			g.addEdge("1", "2", 5);
			g.addEdge("1", "3", 3);
			g.addEdge("2", "3", 4);
			g.addEdge("3", "2", 2);
			g.addEdge("3", "5", 6);
			g.addEdge("4", "6", 1);
			g.addEdge("5", "4", 3);
			g.addEdge("5", "6", 2);
			
			assertEquals("Comparing maxCapacity of found Edge", 6, g.getEdge("3", "5").getMaxCapacity());
			
		} catch (EdgeAlreadyDefinedException e) {
			e.printStackTrace();
		}
	}
	
	@Test (expected=EdgeAlreadyDefinedException.class)
	public void testAddEdgeWithException() throws EdgeAlreadyDefinedException {
		Graph g = new Graph();
		
		g.addEdge("1", "2", 5);
		g.addEdge("1", "2", 3);
		
	}
	
	@Test
	public void testAddEdge() {
		Graph g = new Graph();
		
		try {
			g.addEdge("1", "2", 5);
		} catch (EdgeAlreadyDefinedException e) {
			e.printStackTrace();
			fail();
		}
		
		
	}
	
}
