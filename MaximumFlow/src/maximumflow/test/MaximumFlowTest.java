package maximumflow.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import org.junit.Test;

import maximumflow.*;

public class MaximumFlowTest {

	@Test
	public void testBFSOnNull() {
		
		Graph g = new Graph();
		MaximumFlow mf = new MaximumFlow(g);
		
		try {
			g.addEdge("1", "2", 0);
			g.addEdge("1", "3", 0);
			g.addEdge("2", "3", 0);
			g.addEdge("3", "2", 0);
			g.addEdge("3", "5", 0);
			g.addEdge("4", "6", 0);
			g.addEdge("5", "4", 0);
			g.addEdge("5", "6", 0);
		} catch (EdgeAlreadyDefinedException e) {
			e.printStackTrace();
		}
		
		assertNull("There should be no path from 5 to 1.", mf.BFS("5", "1"));
	}
	
	@Test
	public void testBFSOnActualPath() {
		
		Graph g;
		try {
			g = new GraphImporter().getGraph(new File("Daten11A.txt"));
			
			MaximumFlow mf = new MaximumFlow(g);
			LinkedList<String> actualPath;
			
			actualPath = mf.BFS("6", "17");
			
			assertEquals("6", actualPath.get(0));
			assertEquals("5", actualPath.get(1));
			assertEquals("3", actualPath.get(2));
			assertEquals("13", actualPath.get(3));
			assertEquals("21", actualPath.get(4));
			assertEquals("19", actualPath.get(5));
			assertEquals("2", actualPath.get(6));
			assertEquals("17", actualPath.get(7));
			
			
		} catch (IOException e) {
			e.printStackTrace();
			fail("Problems with File: \n" + e.getStackTrace());
		}
	}
	
	@Test
	public void testMaximumFlowOnSmallGraph() {
		
		Graph g = new Graph();
		
		try {
			g.addEdge("1", "2", 5);
			g.addEdge("1", "3", 3);
			g.addEdge("2", "3", 4);
			g.addEdge("3", "2", 2);
			g.addEdge("3", "5", 6);
			g.addEdge("4", "6", 1);
			g.addEdge("5", "4", 3);
			g.addEdge("5", "6", 2);
			
			MaximumFlow mf = new MaximumFlow(g);
			
			int maxFlow = mf.calculateMaximumFlow("1", "6");
			
			System.out.println(mf.getGraph().toString());
			
			assertEquals("The maximum flow of this Graph is 3.", 3, maxFlow);
		} catch (EdgeAlreadyDefinedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCalculateMaximumFlowOnActualGraph() {
		Graph g;
		try {
			g = new GraphImporter().getGraph(new File("Daten11A.txt"));
			
			MaximumFlow mf = new MaximumFlow(g);
			
			int maxFlow = mf.calculateMaximumFlow("6", "17");
			
			System.out.println("Maximalfluss: " + maxFlow);
			System.out.println("Liste der Benutzten Fluesse");
			System.out.println(mf.getGraph());
			
		} catch (IOException e) {
			e.printStackTrace();
			fail("Problems with File.");
		}
	}
}
